package com.extraem.interview.controller;

import org.springframework.web.bind.annotation.GetMapping;

public class HomeController {

	@GetMapping("/")
	public String home() {
		return "<h1>Home</h1>";
	}
}
