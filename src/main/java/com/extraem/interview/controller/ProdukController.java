package com.extraem.interview.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.extraem.interview.model.Produk;
import com.extraem.interview.service.ProdukService;

@RestController
public class ProdukController {

	@Autowired
	ProdukService produkService;
	
	@GetMapping("/products")
	public List<Produk> getAllProducts(){
		return produkService.getAllProducts();
	}
	
	@GetMapping("/products/{id}")
	public Produk getProduct(@PathVariable long id) {
		return produkService.getProduct(id);
	}
	
	@PostMapping("/products")
	public void createProduct(@RequestBody Produk produk) {
		produkService.createProduct(produk);
	}
	
	@PutMapping("/products/{id}")
	public void updateProduct(@RequestBody Produk produk, @PathVariable long id) {
		produkService.updateProduct(produk, id);
	}
	
	@DeleteMapping("/products/{id}")
	public void deleteProduct(@PathVariable long id) {
		produkService.deleteProduct(id);
	}
}
