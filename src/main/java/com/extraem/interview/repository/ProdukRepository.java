package com.extraem.interview.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.extraem.interview.model.Produk;

public interface ProdukRepository extends JpaRepository<Produk, Long> {

}
