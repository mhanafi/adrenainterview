package com.extraem.interview.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.extraem.interview.model.Produk;
import com.extraem.interview.repository.ProdukRepository;

@Service
public class ProdukService {

	@Autowired
	ProdukRepository produkRepository;
	
	public List<Produk> getAllProducts() {
		return produkRepository.findAll();
	}

	public Produk getProduct(long id) {
		Optional<Produk> produk = produkRepository.findById(id);
		if(produk.isPresent()) {
			return produk.get();
		}
		return null;
	}

	public void createProduct(Produk produk) {
		produkRepository.save(produk);
	}

	public void updateProduct(Produk produk, long id) {
		Optional<Produk> result = produkRepository.findById(id);
		if(result.isPresent()) {
			Produk produkToUpdate = result.get();
			produkToUpdate.setId(id);
			produkToUpdate.setTitle(produk.getTitle());
			produkToUpdate.setDescription(produk.getDescription());
			produkToUpdate.setPrice(produk.getPrice());
			produkRepository.flush();
		}
	}

	public void deleteProduct(long id) {
		produkRepository.deleteById(id);
	}

	
	
}
